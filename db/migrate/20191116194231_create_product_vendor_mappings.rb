class CreateProductVendorMappings < ActiveRecord::Migration[5.2]
  def change
    create_table :product_vendor_mappings do |t|
      t.integer :vendor_id
      t.integer :product_id

      t.timestamps
    end
    add_index :product_vendor_mappings, :vendor_id
    add_index :product_vendor_mappings, :product_id
  end
end
