class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.integer :vendor_id
      t.integer :user_id
      t.integer :product_id
      t.integer :payable_amount_id
      t.integer :product_color_mapping_id
      t.integer :product_size_mapping_id
      t.integer :quantity
      t.decimal :rate

      t.timestamps
    end
    add_index :orders, :vendor_id
    add_index :orders, :user_id
    add_index :orders, :product_id
    add_index :orders, :payable_amount_id
    add_index :orders, :product_color_mapping_id
    add_index :orders, :product_size_mapping_id
  end
end
