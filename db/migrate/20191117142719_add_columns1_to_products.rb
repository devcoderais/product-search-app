class AddColumns1ToProducts < ActiveRecord::Migration[5.2]
  def change
    add_column :products, :rate, :decimal, precision: 64, scale: 12
  end
end
