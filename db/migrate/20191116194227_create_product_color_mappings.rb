class CreateProductColorMappings < ActiveRecord::Migration[5.2]
  def change
    create_table :product_color_mappings do |t|
      t.integer :product_id
      t.integer :color_id

      t.timestamps
    end
    add_index :product_color_mappings, :product_id
    add_index :product_color_mappings, :color_id
  end
end
