class CreateCategories < ActiveRecord::Migration[5.2]
  def change
    create_table :categories do |t|
      t.string :name
      t.text :description
      t.integer :parent_category_id
      t.integer :relevance_score

      t.timestamps
    end
    add_index :categories, :parent_category_id
  end
end
