class CreateUserCategoryProductRelevanceMappings < ActiveRecord::Migration[5.2]
  def change
    create_table :user_category_product_relevance_mappings do |t|
      t.integer :user_id
      t.integer :product_id
      t.integer :category_id
      t.integer :relevance_score

      t.timestamps
    end
    add_index :user_category_product_relevance_mappings, :user_id
    add_index :user_category_product_relevance_mappings, :product_id
    add_index :user_category_product_relevance_mappings, :category_id
  end
end
