class CreateProductSecondaryTitles < ActiveRecord::Migration[5.2]
  def change
    create_table :product_secondary_titles do |t|
      t.string :name
      t.integer :product_id

      t.timestamps
    end
    add_index :product_secondary_titles, :product_id
  end
end
