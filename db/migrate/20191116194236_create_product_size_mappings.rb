class CreateProductSizeMappings < ActiveRecord::Migration[5.2]
  def change
    create_table :product_size_mappings do |t|
      t.integer :product_id
      t.integer :size_id

      t.timestamps
    end
    add_index :product_size_mappings, :product_id
    add_index :product_size_mappings, :size_id
  end
end
