class CreateProductSkus < ActiveRecord::Migration[5.2]
  def change
    create_table :product_skus do |t|
      t.integer :product_id
      t.string :uuid

      t.timestamps
    end
  end
end
