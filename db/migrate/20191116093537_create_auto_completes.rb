class CreateAutoCompletes < ActiveRecord::Migration[5.2]
  def change
    create_table :auto_completes do |t|

      t.timestamps
    end
  end
end
