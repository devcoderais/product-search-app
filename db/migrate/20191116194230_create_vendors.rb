class CreateVendors < ActiveRecord::Migration[5.2]
  def change
    create_table :vendors do |t|
      t.string :trade_name
      t.string :legal_name
      t.boolean :premium
      t.integer :relevance_score

      t.timestamps
    end
  end
end
