class CreateProductGenderMappings < ActiveRecord::Migration[5.2]
  def change
    create_table :product_gender_mappings do |t|
      t.integer :product_id
      t.integer :gender_id

      t.timestamps
    end
    add_index :product_gender_mappings, :product_id
    add_index :product_gender_mappings, :gender_id
  end
end
