class CreateSearchSuggestions < ActiveRecord::Migration[5.2]
  def change
    create_table :search_suggestions do |t|

      t.timestamps
    end
  end
end
