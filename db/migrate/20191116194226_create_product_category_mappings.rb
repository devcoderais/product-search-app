class CreateProductCategoryMappings < ActiveRecord::Migration[5.2]
  def change
    create_table :product_category_mappings do |t|
      t.integer :product_id
      t.integer :category_id

      t.timestamps
    end
    add_index :product_category_mappings, :product_id
    add_index :product_category_mappings, :category_id
  end
end
