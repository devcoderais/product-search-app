# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)




c1 = Color.create(name: "grey")
c2 = Color.create(name: "red")
c3 = Color.create(name: "blue")
c4 = Color.create(name: "brown")
c5 = Color.create(name: "aqua")
c6 = Color.create(name: "yellow")
c7 = Color.create(name: "white")
c8 = Color.create(name: "pink")
c9 = Color.create(name: "dark_grey")
c10 = Color.create(name: "black")


# It can have more parent categories like Men/Women/Kid Dress along with age group scoping
ca_12 = Category.create(name: "Mens Wear")
ca_13 = Category.create(name: "Womens Wear")
ca_14 = Category.create(name: "Kids Wear") 
ca_1 = Category.create(name: "Dress", parent_category_id: ca_14.id)
ca_2 = Category.create(name: "Sleeves Dress", parent_category_id: ca_1.id)
ca_3 = Category.create(name: "Sleeveless Dress", parent_category_id: ca_1.id)
ca_15 = Category.create(name: "Casual Dress", parent_category_id: ca_1.id)

# It can can have more parent categies like, Footwear and can be scoped to age/age-groups
ca_4 = Category.create(name: "Boots")
ca_5 = Category.create(name: "Summer Boots", parent_category_id: ca_4.id)
ca_6 = Category.create(name: "Winter Boots", parent_category_id: ca_4.id)
ca_7 = Category.create(name: "Rain Boots", parent_category_id: ca_4.id)

ca_8 = Category.create(name: "Cap")
ca_9 = Category.create(name: "Golf Cap", parent_category_id: ca_8.id)

ca_10 = Category.create(name: "Socks")
ca_11 = Category.create(name: "Pack Of Two Socks", parent_category_id: ca_10.id)

v1 = Vendor.create(trade_name: "Sundar Textiles")
v2 = Vendor.create(trade_name: "Bhushan Mills")
v3 = Vendor.create(trade_name: "Sriram Mills")
v4 = Vendor.create(trade_name: "Nikita Fashion Store")

s1 = Size.create(name: "Small")
s2 = Size.create(name: "Medium")
s3 = Size.create(name: "Large")




   p1 = Product.create(primary_title: "All Over Print Cap Sleeves Dress")
p1_ca2 = ProductCategoryMapping.create(product_id: p1.id, category_id: ca_2.id)
p1_ca1 = ProductCategoryMapping.create(product_id: p1.id, category_id: ca_1.id)
p1_c2 = ProductColorMapping.create(color_id: c2.id, product_id: p1.id)
p1_c1 = ProductColorMapping.create(color_id: c1.id, product_id: p1.id)
p1_v4 = ProductVendorMapping.create(product_id: p1.id, vendor_id: v4.id)
p1_v1 = ProductVendorMapping.create(product_id: p1.id, vendor_id: v1.id)
p1_s1 = ProductSizeMapping.create(product_id: p1.id, size_id: s1.id)
p1_s2 = ProductSizeMapping.create(product_id: p1.id, size_id: s2.id)




   p2 = Product.create(primary_title: "Lovely Red Sequin Tutu Dress")
   p2_ca1 = ProductCategoryMapping.create(product_id: p2.id, category_id: ca_1.id)
   p2_ca14 = ProductCategoryMapping.create(product_id: p2.id, category_id: ca_14.id)
p2_c3 = ProductColorMapping.create(color_id: c2.id, product_id: p2.id)
p2_v4 = ProductVendorMapping.create(product_id: p2.id, vendor_id: v1.id)
p2_s1 = ProductSizeMapping.create(product_id: p2.id, size_id: s1.id)
p2_s2 = ProductSizeMapping.create(product_id: p2.id, size_id: s2.id)






   p3 = Product.create(primary_title: "Pink Candies Print Bow Applique Dress")
   p3_ca14 = ProductCategoryMapping.create(product_id: p3.id, category_id: ca_14.id)
p3_c8 = ProductColorMapping.create(color_id: c8.id, product_id: p3.id)
p3_v4 = ProductVendorMapping.create(product_id: p3.id, vendor_id: v3.id)
p3_s1 = ProductSizeMapping.create(product_id: p3.id, size_id: s1.id)
p3_s2 = ProductSizeMapping.create(product_id: p3.id, size_id: s2.id)
p3_s3 = ProductSizeMapping.create(product_id: p3.id, size_id: s3.id)



   p4 = Product.create(primary_title: "Aqua Casual Dress")
p4_c5 = ProductColorMapping.create(color_id: c5.id, product_id: p4.id)
p4_c3 = ProductColorMapping.create(color_id: c3.id, product_id: p4.id)
p4_c1 = ProductColorMapping.create(color_id: c1.id, product_id: p4.id)

p4_v4 = ProductVendorMapping.create(product_id: p4.id, vendor_id: v2.id)
p4_s1 = ProductSizeMapping.create(product_id: p4.id, size_id: s1.id)
p4_ca15 = ProductCategoryMapping.create(product_id: p4.id, category_id: ca_15.id)
p4_ca14 = ProductCategoryMapping.create(product_id: p4.id, category_id: ca_14.id)



# All Over Print Cap Sleeves Dress - Red
# Stylish Floral Print Sleeveless Mid Thigh Length Dress With Hat
# Lovely Red Sequin Tutu Dress
# Pink Candies Print Bow Applique Dress
# Aqua Casual Dress
# Brown High Ankle Winter Boots
# Yellow Cartoon Print Rain Boots
# Blue Bear Clogs
# Blue Gray Strips Design Mesh Slip On Shoes
# Blue Sailor Themed Flip Flops
# White And Black LED Shoes With Stripes
# Black Waist High Stockings With Bows
# White, Pink Butterfly Lace Socks - Pack Of 2
# Dark Gray Crochet Golf Cap Beanie
# Grey Cap With Ear Flaps
