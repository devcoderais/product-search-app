Rails.application.routes.draw do
  resources :search_suggestions
  root "products#index"
  resources :auto_completes do
    get 'search_suggestions'
  end
  resources :products
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
