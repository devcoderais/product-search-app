require "application_system_test_case"

class AutoCompletesTest < ApplicationSystemTestCase
  setup do
    @auto_complete = auto_completes(:one)
  end

  test "visiting the index" do
    visit auto_completes_url
    assert_selector "h1", text: "Auto Completes"
  end

  test "creating a Auto complete" do
    visit auto_completes_url
    click_on "New Auto Complete"

    click_on "Create Auto complete"

    assert_text "Auto complete was successfully created"
    click_on "Back"
  end

  test "updating a Auto complete" do
    visit auto_completes_url
    click_on "Edit", match: :first

    click_on "Update Auto complete"

    assert_text "Auto complete was successfully updated"
    click_on "Back"
  end

  test "destroying a Auto complete" do
    visit auto_completes_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Auto complete was successfully destroyed"
  end
end
