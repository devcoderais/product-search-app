require "application_system_test_case"

class SearchSuggestionsTest < ApplicationSystemTestCase
  setup do
    @search_suggestion = search_suggestions(:one)
  end

  test "visiting the index" do
    visit search_suggestions_url
    assert_selector "h1", text: "Search Suggestions"
  end

  test "creating a Search suggestion" do
    visit search_suggestions_url
    click_on "New Search Suggestion"

    click_on "Create Search suggestion"

    assert_text "Search suggestion was successfully created"
    click_on "Back"
  end

  test "updating a Search suggestion" do
    visit search_suggestions_url
    click_on "Edit", match: :first

    click_on "Update Search suggestion"

    assert_text "Search suggestion was successfully updated"
    click_on "Back"
  end

  test "destroying a Search suggestion" do
    visit search_suggestions_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Search suggestion was successfully destroyed"
  end
end
