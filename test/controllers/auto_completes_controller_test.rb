require 'test_helper'

class AutoCompletesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @auto_complete = auto_completes(:one)
  end

  test "should get index" do
    get auto_completes_url
    assert_response :success
  end

  test "should get new" do
    get new_auto_complete_url
    assert_response :success
  end

  test "should create auto_complete" do
    assert_difference('AutoComplete.count') do
      post auto_completes_url, params: { auto_complete: {  } }
    end

    assert_redirected_to auto_complete_url(AutoComplete.last)
  end

  test "should show auto_complete" do
    get auto_complete_url(@auto_complete)
    assert_response :success
  end

  test "should get edit" do
    get edit_auto_complete_url(@auto_complete)
    assert_response :success
  end

  test "should update auto_complete" do
    patch auto_complete_url(@auto_complete), params: { auto_complete: {  } }
    assert_redirected_to auto_complete_url(@auto_complete)
  end

  test "should destroy auto_complete" do
    assert_difference('AutoComplete.count', -1) do
      delete auto_complete_url(@auto_complete)
    end

    assert_redirected_to auto_completes_url
  end
end
