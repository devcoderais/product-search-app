class AutoCompletesController < ApplicationController
  before_action :set_auto_complete, only: [:show, :edit, :update, :destroy]

  # GET /auto_completes
  # GET /auto_completes.json
  def index
    @auto_completes = AutoComplete.all
    respond_to do |format|
      format.js
    end
    # render json: %w[foo bar]
  end
 
  # GET /auto_completes/1
  # GET /auto_completes/1.json
  def show
  end

  # GET /auto_completes/new
  def new
    @auto_complete = AutoComplete.new
  end

  # GET /auto_completes/1/edit
  def edit
  end

  # POST /auto_completes
  # POST /auto_completes.json
  def create
    @auto_complete = AutoComplete.new(auto_complete_params)

    respond_to do |format|
      if @auto_complete.save
        format.html { redirect_to @auto_complete, notice: 'Auto complete was successfully created.' }
        format.json { render :show, status: :created, location: @auto_complete }
      else
        format.html { render :new }
        format.json { render json: @auto_complete.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /auto_completes/1
  # PATCH/PUT /auto_completes/1.json
  def update
    respond_to do |format|
      if @auto_complete.update(auto_complete_params)
        format.html { redirect_to @auto_complete, notice: 'Auto complete was successfully updated.' }
        format.json { render :show, status: :ok, location: @auto_complete }
      else
        format.html { render :edit }
        format.json { render json: @auto_complete.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /auto_completes/1
  # DELETE /auto_completes/1.json
  def destroy
    @auto_complete.destroy
    respond_to do |format|
      format.html { redirect_to auto_completes_url, notice: 'Auto complete was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def search_suggestions
    Product.all
    respond_to do |format|
      format.js
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_auto_complete
      @auto_complete = AutoComplete.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def auto_complete_params
      params.fetch(:auto_complete, {})
    end
end
