json.extract! auto_complete, :id, :created_at, :updated_at
json.url auto_complete_url(auto_complete, format: :json)
