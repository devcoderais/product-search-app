class ProductSecondaryTitle < ApplicationRecord
  belongs_to :product, optional: true
end
