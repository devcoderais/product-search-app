class ProductSku < ApplicationRecord
  belongs_to :product, optional: true
  belongs_to :product_category_mapping, optional: true
  belongs_to :product_color_mapping, optional: true
  belongs_to :product_vendor_mapping, optional: true
  belongs_to :product_gender_mapping, optional: true
  belongs_to :product_size_mapping, optional: true
  belongs_to :buyer, class_name: "User", optional: true
end