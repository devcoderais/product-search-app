class Product < ApplicationRecord

  has_many :product_secondary_titles, dependent: :destroy
  has_many :product_category_mappings
  has_many :categories, through: :product_category_mappings
  has_many :product_color_mappings
  has_many :colors, through: :product_color_mappings
  has_many :product_vendor_mappings
  has_many :vendors, through: :product_vendor_mappings
  has_many :product_size_mappings
  has_many :sizes, through: :product_size_mappings
end
