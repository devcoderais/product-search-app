class Order < ApplicationRecord
  belongs_to :vendor, optional: true
  belongs_to :user, optional: true
  belongs_to :product, optional: true
  belongs_to :payable_amount, class_name: "Amount", optional: true
  belongs_to :product_color_mapping, optional: true
  belongs_to :product_size_mapping, optional: true
end
