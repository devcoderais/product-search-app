class ProductGenderMapping < ApplicationRecord
  belongs_to :product, optional: true
  belongs_to :gender, optional: true
end
