class ProductVendorMapping < ApplicationRecord
  belongs_to :product, optional: true
  belongs_to :vendor, optional: true
end
